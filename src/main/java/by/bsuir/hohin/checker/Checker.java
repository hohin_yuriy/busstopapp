package by.bsuir.hohin.checker;

import java.util.List;

/**
 * A common interface for checking restrictions
 * that was imposed by condition
 * @param <T> - entity that we want to check
 */
public interface Checker<T> {
    public Boolean canProvide(List<T> entitiesList, T entity);
}
