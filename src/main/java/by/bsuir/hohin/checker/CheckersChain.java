package by.bsuir.hohin.checker;

import java.util.List;

/**
 * This class represents a chain of Responsibility pattern
 * for easy way to check condition restrictions
 * @param <T> - entity for checking
 */
public abstract class CheckersChain<T> implements Checker<T> {

    private Checker<T> next;

    /**
     * This method links of chain item with other
     * @param next next item of checkers chain
     */
    public CheckersChain<T> linkWith(CheckersChain<T> next) {
        this.next = next;
        return next;
    }

    /**
     * Invokes next chain item if it non null
     */
    protected boolean checkNext(List<T> entitiesList, T entity) {
        if (next == null) {
            return true;
        }
        return next.canProvide(entitiesList, entity);
    }
}

