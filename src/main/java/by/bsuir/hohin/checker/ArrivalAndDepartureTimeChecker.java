package by.bsuir.hohin.checker;

import by.bsuir.hohin.entity.Bus;
import by.bsuir.hohin.entity.ServiceName;

import java.util.List;

/**
 * Checks efficency of bus trip by 4 params:
 * 1) Starts same time reaches erlier
 * 2) starts later reaches the same time
 * 3) starts later and reaches erlier
 * 4) all the same but Posh better than Grotty :)-
 */
public class ArrivalAndDepartureTimeChecker extends CheckersChain<Bus> implements Checker<Bus> {

    @Override
    public Boolean canProvide(List<Bus> buses, Bus bus) {
        for (Bus b : buses) {
            if (bus.getServiceName().equals(ServiceName.GROTTY)
                    && b.getServiceName().equals(ServiceName.POSH)
                    && b.getDepartureTime().equals(bus.getDepartureTime())
                    && b.getArrivalTime().equals(bus.getArrivalTime())) {
                return false;
            }
            if (b.getDepartureTime().equals(bus.getDepartureTime())
                    && b.getArrivalTime().compareTo(bus.getArrivalTime()) < 0) {
                return false;
            }
            if (b.getDepartureTime().compareTo(bus.getDepartureTime()) > 0
                    && b.getArrivalTime().compareTo(bus.getArrivalTime()) <= 0) {
                return false;
            }
        }
        return checkNext(buses, bus);
    }
}
