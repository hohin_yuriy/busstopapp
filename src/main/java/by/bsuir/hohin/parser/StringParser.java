package by.bsuir.hohin.parser;

import java.text.ParseException;

/**
 * Commont interface for parsing entity from string
 * @param <T> - parsing entity
 */
public interface StringParser<T> {
    public T parse(final String info);
}
