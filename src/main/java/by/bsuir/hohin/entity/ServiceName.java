package by.bsuir.hohin.entity;

/**
 * This enum contains a bus companies
 * that provides shipping activity
 */
public enum ServiceName {
    GROTTY("Grotty"),
    POSH("Posh");

    private final String name;

    private ServiceName(String name) {
        this.name = name;
    }

    /**
     * This method helps us to get a value of
     * this enum parsing value in String format
     * @param text - a value we want to parse
     * @return ServiceName identity
     */
    public static ServiceName fromString(String text) {
        for (ServiceName serviceName : ServiceName.values()) {
            if (serviceName.name.equalsIgnoreCase(text)) {
                return serviceName;
            }
        }
        return null;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
